//FETCH
// type texte
fetch("fakers/info.txt")
.then(
    (response)=>(response.text())
).then(
   // (info)=>console.log(info)
);


//type json
fetch("fakers/info.json")
.then(
    (response)=>(response.json())
).then(
//(info)=>console.log(info)
);

// OPTION REQUETE: CRUD => creat(POST), read(GET), update(PUT), delete(DELETE)
//COMMENT PASSER UN OBJET D'INFORMATION A FETCH ?

//01 GET

const initGet = {
    method: 'GET',
    headers: new Headers(),
    mode: 'cors',
    cache: 'default'
}

fetch("fakers/info.json", initGet)
.then(
 //   (response)=>console.log(response)
)

//02 POST
//COMMENT POSTER DES DONNEES SUR UN SERVEUR SANS BACKOFFICE ?
/*
- INSTALL NODEJS
- $ npm -v
- $ cd data
- $ npm init -y
- Install JSON Server: $ npm install -g json-server
- Create a db.json
- Start JSON Server: $ json-server --watch db.json
- POST
*/

const initPost = {
    method: 'POST',
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify(
        {
            pseudo: "ceci est mon nom",
            message: "ceci est un message les gens!"
        }
    ),
    mode: 'cors',
    credentials: 'same-origin',
}


    document.querySelector("form").addEventListener("submit", function () {
        fetch("http://localhost:3000/users", initPost)
        .then(
    //    console.log("ok")
    )

})

//03 DELETE
//recuperer ID

const initDelete = {
    method: 'DELETE',
    headers: {
        "Content-Type": "application/json"
    },
    mode: 'cors',
    credentials: 'same-origin',
}


    document.querySelector("form").addEventListener("submit", function () {
        fetch("http://localhost:3000/posts/4" , initDelete)
        .then(
        // console.log("ok")
    )

})

//- L'asynchrone

setTimeout(function () {
//    console.log("okk");
}, 2000
);

//async/await

async function fetchData() {
    await fetch ("monLien")
    executeFonction();
}



fetch('monLien').then(
 //   (res)=>console.log(res.text())

)

//JSON
// methode .json() : transformer un format json pour qu il soit interprété
// methode .stringify() : transformer json en chaine de caractere
// methode .parse() : transformer json en objet js


fetch("fakers/info.json").then(
    (response) => (response.json()).then(
        (info)=> {
           // console.log(JSON.stringify(info))

            let settings = JSON.stringify(info)
            console.log(JSON.parse(settings))
        }
    )
)

//web API
//local storage (long terme)

//localStorage.data = "je stock de la data"
//document.body.textContent = localStorage.data;

// localStorage.removeItem("data")

//session storage (court terme)
sessionStorage.dataSettings = "55px";

//cookies :  c est envoyer d un serveur vers un navigateur (stocké par un navigateur)
 

